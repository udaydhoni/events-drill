let containerOfBoxes = document.querySelector('.container');
let mainHeading = document.querySelector('h1');
let mainHeader = document.querySelector('header');

mainHeader.style.height = '100px';


// Styling for mainHeading

mainHeading.style.textAlign = 'center';

//Styling for containerOfBoxes

containerOfBoxes.style.display = 'flex';
containerOfBoxes.style.padding = '10px';
containerOfBoxes.style.flexWrap = 'wrap';
containerOfBoxes.style.margin = '0px';



// Inserting 500boxes in containerOfBoxes

for (let count=0;count<500;count++) {
    let box = document.createElement('div');
    box.style.width = '4rem';
    box.style.height = '4rem';
    box.style.margin= '0px';
    box.style.border = '1px solid black';
    box.classList.add('floor');
    containerOfBoxes.append(box);

}

containerOfBoxes.addEventListener('mouseover', ()=>{
    mainHeader.style.background = 'black';
    let floorItems = document.querySelectorAll('.floor');
    Array.from(floorItems).forEach((elem)=>{
        elem.textContent = Math.floor(Math.random()*500);
        let x = Math.random()*360;
        elem.style.background = `hsl(${x},100%,50%)`
    })
})

mainHeader.addEventListener('mouseover',()=>{
    mainHeader.style.background = 'white';
    let floorItems = document.querySelectorAll('.floor');
    Array.from(floorItems).forEach((elem)=>{
        elem.textContent = Math.floor(Math.random()*500);
        //let x = Math.random()*360;
        elem.style.background = 'white';
    })
})

